<?php
#########################################################################################
## 	Purpose: 		Allows a user to edit a radfordpedia information page.
##					Will retrieve a page provided via the $_GET array, if exists in DB.
##					-	If page does not exist in DB, will provide blank edit field, and 
##						populate page title field with the page title provided.
##					-	If page exists in DB, will populate the edit fields with the
##						information for that page inside the database.
##					-	If no page provided, will load default values for the edit box.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	04 December 2017
#########################################################################################
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
require_once('radfordpedia-utils.php');
require_once('db-connection.php');

//Start session
session_start();


//Retrieve page from GET
$page = getGet('page');

//Initiate Database Connection
$dbConn = db_connect_radfordpedia();

//Initialize page body creation
$pageBody = "";

if (safeLookup($_SESSION, 'username') == FALSE){ //Check to see if user is logged in
	$pageBody = makeMaster();
	$pageBody .= "<span class='ErrMsg'>You must be <a href='login.php'>logged in</a> in order to edit a page.</span>";
	$pageTitle = "Please log in to edit a page";
} else{
	//Maybe also check if user exists in database (in case of session jacking?)
	if (pageExists($dbConn, $page)){
		$pageInfo = findPage($dbConn, $page);
		$pageTitle = safeLookup($pageInfo, 'PageTitle');
		$editboxvalue = safeLookup($pageInfo, 'PageBody');
	} else{
		$pageTitle = 'Page Title Goes Here';
		$editboxvalue = "Write something interesting. AND MAKE IT GOOD!";
	}
	$pageBody = makeMaster();
	$pageBody .= "
	<div style='float:left; width:40%; border-width: 1px;'>
		<FORM action='edit-handle.php' method='post'>
			<fieldset>
			<legend>Edit Page</legend>
			<B>EDIT TITLE</B><BR/>
			<input type='text' name='pagetitle' id='pagetitle' maxlength='50' size='60' value='" . ($page == "" ? "New Page Title Here" : $page) . "'><BR/>
			<B>EDIT PAGE</B><BR/>
			<textarea name='pagebody' name='pagebody' id='pagebody' cols='60' rows='30'>" . $editboxvalue . "</textarea><BR/>
			<input type='submit' name='edit-submission'>
			<input type='hidden' name='NewPage' value = 'true'>
			</fieldset>
		</FORM>
	</div>
	<div style='float:left; width:40%; border-width: 1px;'>"
		.  displayEditHelp() . "</div>";
}


?>

<HTML>
	<HEAD>
		<TITLE><?php echo "Edit: $pageTitle" ?></TITLE>
		<link rel="stylesheet" href="styles.css">
	</HEAD>
	<BODY>
		<?php echo $pageBody ?>
	</BODY>
</HTML>