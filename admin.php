<!DOCTYPE html>
<html>
	<head>
		<?php session_start() ?>
		<?php error_reporting(E_ALL); ?>
		<?php require_once("db-connection.php"); ?>
		<?php require_once("radfordpedia-utils.php"); ?>
		<?php
			$connection = db_connect_radfordpedia();
			if(!checkIfAdmin($connection, $_SESSION["username"]))
			{
				mysqli_close($connection);
				header('Location: viewpage.php'); 
			}
			else
			{
				mysqli_close($connection);
			}

		?>
		<link rel="stylesheet" href="styles.css">
		<script src="script.js"></script>
		<title>Admin Page</title>
	</head>
	<body id="body">
		<?php echo makeMaster(); ?>
		<center><div>
			<h1>Users</h1>
			<?php echo getUsersTable(); ?>
		</div></center>
		<center><div>
			<h1>Posts</h1>
			<?php echo getPostsTable(); ?> 
		</div></center>
		
	</body>
</html>