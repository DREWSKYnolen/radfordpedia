<?php
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
require_once('radfordpedia-utils.php');
session_start();

$logonStatus = getGet('logon');
$logonFailure = "<SPAN style='background-color: salmon'>Sorry, your username/password combo was not found.</SPAN>";

?>
<HTML>
<HEAD>
	<TITLE>Signin Page</TITLE>
	<link rel="stylesheet" href="styles.css">
</HEAD>
<BODY>
	<?php echo makeMaster();?>
<form id='login' action='login-handle.php' method='post'>
		<fieldset>
		<legend>Sign In</legend>

		<label for='username'>UserName</label>
		<input type='text' name='username' id='username' maxlength="50" />

		<label for="password">Password</label>
		<input type='password' name='password' id='password' maxlength="50" />

		<input type="submit" name="submit" value="Submit" />
		<?php if ($logonStatus == 'fail') echo $logonFailure; ?>
		</fieldset>

	</form>
</BODY>
</HTML>