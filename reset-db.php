<?php

#########################################################################################
## 	Purpose: 		To initialize the radfordpedia database.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	30 November 2017
#########################################################################################


error_reporting(E_ALL);
require_once('db-connection.php'); //For connection functions
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);

//open Radfordpedia database connection
$connection = db_connect_radfordpedia();
if ($connection){
	echo "Database reset!<BR>";
} else{
	echo "Database connection failed.";
}
echo "<A HREF='index.php'>Back to landing</A>";

//Strings for table creations
$createPageTable = <<<ETBL
	CREATE TABLE Pages(
		PageTitle 	VARCHAR(50),
		PRIMARY KEY(PageTitle)
	)
ETBL;

$createPageEditTable = <<<EDTBL
	CREATE TABLE PageEdits(
		PageTitle 	VARCHAR(50),
		PageBody	VARCHAR(10000),
		UserName 	VARCHAR(15),
		EditDate 	DATETIME,
		PRIMARY KEY(PageTitle, EditDate),
		FOREIGN KEY(UserName) REFERENCES User
	)
EDTBL;

$createUserTable = <<<USRTBL
	CREATE TABLE IF NOT EXISTS `User` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `fName` varchar(50) NOT NULL,
  `lName` varchar(50) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `admin` int(11) NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;
USRTBL;

//Drop current tables
mysqli_query($connection, "DROP TABLE Pages");
mysqli_query($connection, "DROP TABLE User");
mysqli_query($connection, "DROP TABLE PageEdits");

//Create Tables
mysqli_query($connection, $createPageTable);
mysqli_query($connection, $createUserTable);
mysqli_query($connection, $createPageEditTable);

//Insert initial page (For testing purposes)
$rpediaTitle = "RadfordPedia";
$rpediaBody = "This is a basic page with no weird issues which should be inserted with zero trouble.";
mysqli_query($connection, "INSERT INTO Pages (PageTitle, PageBody) VALUES ('$rpediaTitle', '$rpediaBody')");
//echo "INSERT INTO Pages (PageTitle, PageBody) VALUES ('$rpediaTitle', '$rpediaBody')

//Insert initial users to DB
$addUsers = <<<ADDUSR
	INSERT INTO `User` (`UserID`, `fName`, `lName`, `userName`, `email`, `password`, `admin`) VALUES
	(1, 'Ben', 'Adams', 'BigBen', 'badams5@radford.edu', 'blahbah', 1),
	(6, 'Drew', 'Nolen', 'Drewsky', 'anolen3@radford.edu', 'Letmeinplz!', 1),
	(4, 'Bilbo', 'Baggins', 'bbaggins69', 'bbaggins69@radford.edu', 'mine', 0),
	(5, 'Michael', 'Taylor', 'mtaylor131', 'mtaylor131@radford.edu', '12345', 1),
	(7, 'Johnny', 'Test', 'jtest123', 'jtest123@radford.edu', 'iTest', 0),
	(8, 'Harry', 'Frank', 'CapnMal', 'hfrank3@radford.edu', 'Shiny!', 1);
ADDUSR;

mysqli_query($connection, $addUsers);
?>