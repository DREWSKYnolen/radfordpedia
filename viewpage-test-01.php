<?php
//Error reporting and includes
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);

//Test with an empty get array (Expected behavior: Default page of "No page selected, choose from these")
$_GET = array();
require_once('viewpage.php');
?>