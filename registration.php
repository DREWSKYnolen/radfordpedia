<!DOCTYPE html>
<html>
	<head>
		<?php require_once("radfordpedia-utils.php") ?>
		<link rel="stylesheet" href="styles.css">
		<script src="script.js"></script>
		<title>Radfordpedia Registration</title>
	</head>
	<body id="body">
		<?php echo makeMaster(); ?>
		<form action="registrationHandler.php" method="POST" id="registrationForm">
			<table id="content">
				<tr>
					<td colspan="2"><div class="innerDiv">Register For An Account!</div></td>
				</tr>
				<tr>
					<td>First Name:</td><td><input maxlength="50" required="required" type="text" name="firstName" placeholder="First Name"></td>
				</tr>
				<tr>
					<td>Last Name:</td><td><input maxlength="50" required="required" type="text" name="lastName" placeholder="Last Name"></td>
				</tr>
				<tr>
					<td>Username:</td><td><input maxlength="50" required="required" type="text" name="username" placeholder="Username"></td>
				</tr>
				<tr>
					<td>Radford Email Address:</td><td><input pattern=".+@radford.edu" maxlength="50" required="required" type="text" name="emailAddress" placeholder="Email Address"></td>
				</tr>
				<tr>
					<td>Password:</td><td><input required="required" type="password" name="password" placeholder="Password"></td>
				</tr>
				<tr>
					<td colspan="2"><div class="innerDiv"><button id='registerButton' type='submit'>Register</button></div></td>
				</tr>
			</table>
		</form>
	</body>
</html>
