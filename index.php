<?php

#########################################################################################
## 	Purpose: 		A temporary landing page for radfordpedia, to show recently created
##						pages, and link to database reset page.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	30 November 2017
#########################################################################################
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
error_reporting(E_ALL);
require_once('db-connection.php');
?>

<HTML>
	<HEAD>
		<TITLE>Radfordpedia Landing Page</TITLE>
	</HEAD>

	<BODY>
		<H1>Important Files:<BR/></H1>
		<UL>
			<LI><A HREF="edit-page.php">Edit Radfordpedia Page</A></LI>
			<LI><A HREF="reset-db.php">Reset Database</A></LI>
			<LI><A HREF="admin.php">Admin Page</A></LI>
			<LI><A HREF="viewpage.php">View Page Page</A></LI>
		</UL>
		<UL>
			<LI><A HREF="viewpage-test-01.php">Test 01 (Sends no page ID to viewpage)</A></LI>
			<LI><A HREF="viewpage-test-02.php">Test 02 (Tells Viewpage.php to load Radfordpedia)</A></LI>
			<LI><A HREF="viewpage-test-03.php">Test 03 (Tells Viewpage.php to load a page which doesn't exist.)</A></LI>
		</UL>
	</BODY>
</HTML>