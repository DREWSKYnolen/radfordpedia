<?php
//Error reporting and includes
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);

//Test with a page which does not exist in array. Expect page not found, with listing of other pages.
$_GET = array('page' => 'ThisD0ntexist');
require_once('viewpage.php');
?>