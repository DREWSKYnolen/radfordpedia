<?php

session_start();
unset($_SESSION);
session_destroy();
session_write_close();
setcookie( session_name(), '', 1, '/' );
header('Location: viewpage.php'); 

?>

<!DOCTYPE html>
<html>
<head>
	<title>Successful logout</title>
</head>
<body>
	You are now logged out! Huzzah!
</body>
</html>