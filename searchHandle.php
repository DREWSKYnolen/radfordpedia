<?php
error_reporting(E_ALL);
require_once('radfordpedia-utils.php');
require_once('db-connection.php');
session_start();
$username = safeLookup($_SESSION, 'username');
$dbConn = db_connect_radfordpedia();

echo makeMaster();


?>
<br/>

<html>
<head>
	<link rel="stylesheet" href="styles.css" type="text/css">
	<title>Search Results</title>
</head>
<body>
<?php
	$searchTerm = sqlSanitize($dbConn,$_POST['search']);

	$query = "SELECT * FROM  `proj2`.`PageEdits` WHERE (`PageTitle` LIKE  '%$searchTerm%' OR `PageBody` LIKE  '%$searchTerm%') GROUP BY `PageTitle` LIMIT 0 , 5";

	//echo $query . "<br/>";
	

	$searchResults = mysqli_query($dbConn, $query);
	
	if(mysqli_num_rows($searchResults) === 0)
	{
		echo "<h3>Your search for '$searchTerm' returned no results. </h3>";
	}
	else
	{
		while ($oneRow = mysqli_fetch_assoc($searchResults)) {
    	$results[] = $oneRow;
		}

	//echo "<h1>Welcome to RadfordPedia! Check out some of these recently edited articles!</H1><BR/>";
	echo listArticles($results);
	}
	echo "<BR><A HREF='edit-page.php'>Didn't find what you're looking for? Create your own!</A><BR/>";

	mysqli_close($dbConn);
?>

</body>
</html>