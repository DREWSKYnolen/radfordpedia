<?php
#########################################################################################
## 	Purpose: 		- Handles submission of data from edit-page.php
##					- Will ensure data matches submission standards
##					- Will check to see if page exists in database, if so, will UPDATE
##						- If not, will insert new page record into the database.
##					- Handles sanitizing inputs before submitting to DB.
##					- After submitting, will redirect to display the page which was just
##						edited.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	04 December 2017
#########################################################################################

error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
require_once('radfordpedia-utils.php');
require_once('db-connection.php');
session_start();

$pageTitle = getPost('pagetitle');
$pageBody = getPost('pagebody');
$username = safeLookup($_SESSION, 'username');

//NEED ERROR CHECKING HERE
function allErrorMessages($formData, $dbConn){
	$errs = array();
	if (!$dbConn){
		$errs['DBConn'] = "Unable to connect to database: " . mysqli_connect_error();
	} else{
		//User checking
		if (safeLookup($_SESSION, 'username') == FALSE){
			$nextMsg = "You must be logged in to edit page. How'd you get here?";
		} else if (!userExists($dbConn, safeLookup($_SESSION, 'username'))){
			$nextMsg = "This user does not exist. IMPOSTER!";
		} else {
			$nextMsg = FALSE;
		}
		if ($nextMsg) $errs['User'] = $nextMsg;

		//Title Checking
		$nextMsg = getTitleErrors(getPost('pagetitle'));
		if ($nextMsg) $errs['PageTitle'] = $nextMsg;

		//If "new" page, see if already in DB

		//Body Checking

		//String test

		//Blank body test

		//Syntax tests
		$nextMsg = getSyntaxErrors(getPost('pagebody'));
		if ($nextMsg) $errs['Syntax'] = $nextMsg;

		//Somebody else editing?


		//User authorized to edit?

		//Title != Loaded title (Trying to force page changes for other page)

	}
	return $errs;
}

//Trim all post values
foreach ($_POST AS $key=>$val) {
	if (is_string($val)) { $_POST[$key] = trim($val); }
}

//Open connection
$dbConn = db_connect_radfordpedia();

$errs = array_map("strToHtml",allErrorMessages($_POST, $dbConn));

//########################################################################################################################################################
//#### ERROR CHECKING NEEDED:
//####	- Ensure page HAS a title
//###	- Ensure page title is valid
//###	- Make sure that the new title of the page matches the old one (Prevent people from modifying the wrong page.)
//###		- Needs to take into consideration if the user is creating a new page or not. If so, just make sure new title does not exist already.
//###	- Check to make sure that the old text body matches what's currently in the database (Preventing multiple people editing at once)
//###	- Rather than displaying what the user entered, make a "Thank you for your submission" page which either displays errors, or gives a link to it.
//########################################################################################################################################################

if (!$errs) insertPage($dbConn, $pageTitle, $pageBody, !pageExists($dbConn, $pageTitle), $username);

//Close database connection
if ($dbConn) mysqli_close($dbConn);
?>

<HTML>
	<head>
		<link rel="stylesheet" href="styles.css">
		<title><?php echo htmlspecialchars($pageTitle); ?></title>
	</head>
	<BODY>
		<?php echo makemaster(); ?>
		<? if ($errs) {
			echo "<p class='error-message'>\n" . stringsToUl($errs) . "\n</p><hr/>";
			}else header("Location: viewpage.php?page=$pageTitle"); ?>
		<H1>Information entered for the page <?php echo $pageTitle; ?></H1><HR/>
			<?php echo htmlspecialchars($pageBody); ?>
	<HR/><A HREF="viewpage.php">BACK TO LANDING</A>
	<?php //header('Location: viewpage.php?page=' . urlencode($pageTitle)); ?>
	</BODY>