<?php

#########################################################################################
## 	Purpose: 		Contains functions specific to connecting to the 
## 					Radfordpedia database.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	04 December 2017
#########################################################################################

require_once('radfordpedia-utils.php');

/**********************************************
**	Function: 	db_connect_radfordpedia
**	@param:		none
**	@return:	mysqli connection for radfordpedia database -OR- FALSE if failed connection. (Maybe edit to return error message?)
**	Purpose:	Will attempt to connect to the radfordpedia database.
**********************************************/
function db_connect_radfordpedia(){
	// Temporary storage of usernames and passwords, will be migrated to a separate file later.
	$rpedia_hostname = "localhost";
	$rpedia_username = "proj2";
	$rpedia_password = "Security1#";
	$rpedia_schema = "proj2";

	$rpedia_connection = mysqli_connect($rpedia_hostname, $rpedia_username, $rpedia_password, $rpedia_schema); //Connect to the database using credentials
	return $rpedia_connection;
}

function sqlSanitize($connection, $str){
	//Decode HTMLspecialChars
	$str = htmlspecialchars_decode($str);
	//SQL escape string
	$str = mysqli_real_escape_string($connection, $str);
	return $str;
}

/** Function:	userLogin
**	Purpose:	to check to see if a user and password combo exist in database.
**	Parameter:	db connection, username, password
**	Return:		TRUE if successful find, FALSE if not
*/
function userLogin($connection, $username, $password){
	//Call SQL sanitize here, or earlier?
	$query = "SELECT * FROM User WHERE userName = '$username' AND password = '$password'";
	//echo $query;
	$res = mysqli_query($connection, $query);
	return (mysqli_num_rows($res) == 1);
}

/** Function:	userExists
**	Purpose:	to check to see if a user exists in the database
**	Parameter:	db connection, username, 
**	Return:		TRUE if successful find, FALSE if not
*/
function userExists($connection, $username){
	//Call SQL sanitize here, or earlier?
	$query = "SELECT * FROM User WHERE userName = '$username'";
	//echo $query;
	$res = mysqli_query($connection, $query);
	return (mysqli_num_rows($res) == 1);
}

/** Inserts a page into the database **/
////////////////MODIFICATIONS
///////////////Add Option for implementation of Username.
///////////////Username should probably have its own getCurrentUser php function.
function insertPage($connection, $pageTitle, $pageBody, $newPage = FALSE, $userName = 'anonymous'){
	//Sanitize the input for DB insertion
	$pageTitle = sqlSanitize($connection, $pageTitle);
	$pageBody = sqlSanitize($connection, $pageBody);
	//Insert some sort of username check here, or have handler check that before passing username over.
	if ($newPage){
		mysqli_query($connection, "INSERT INTO Pages (PageTitle) VALUES ('$pageTitle')");
	}
	$query = "INSERT INTO PageEdits (PageTitle, PageBody, EditDate, UserName) VALUES ('$pageTitle', '$pageBody', CURRENT_TIMESTAMP, '$userName')";
	$insert = mysqli_query($connection, $query);
	mysqli_query($connection, "INSERT INTO Pages (PageTitle) VALUES ('$pageTitle')");
}

/* Removes an edit by a user (takes pagetitle and editdate and user for triple enforcement.) */
function removeEdit($connection, $PageTitle, $EditDate, $UserName){
	$PageTitle = sqlSanitize($connection, $PageTitle);
	$EditDate = sqlSanitize($connection, $EditDate);
	$UserName = sqlSanitize($connection, $UserName);
	$query = "DELETE FROM PageEdits WHERE PageTitle = '$PageTitle' AND EditDate = '$EditDate' AND UserName = '$UserName'";
	mysqli_query($connection, $query);
	$error = mysqli_error($connection);

	//Assuming no error (Can check here), test to see if there are any edits left. If not, delete page from page table.
	if (!findPage($connection, $PageTitle)){ //Means could not find any more edits
		$query = "DELETE FROM Pages WHERE PageTitle = '$PageTitle'";
		mysqli_query($connection, $query);
		$error .= mysqli_error($connection);
		return $error;
	}else{
		return $error;
	}
}

/* Returns whether or not the specified pageTitle is found in the Pages database.
*/
function pageExists($connection, $pageTitle){
	$pageTitle = sqlSanitize($connection, $pageTitle); //Sanitize for SQL query
	$query = "SELECT * FROM Pages WHERE PageTitle = '$pageTitle'";
	$result = mysqli_query($connection, $query);
	
	return (hasZeroRows($result) ? FALSE : TRUE );
}


/** Takes a page title, and gets the most current version of the page in the database.
**	RETURNS:	Array with the row association for the single row returned.
**
**	Modifications:	Potentially add functionality to choose which fields are returned. This way data which isn't needed isn't passed around.
**/
function findPage($connection, $pageTitle){
	$pageTitle = sqlSanitize($connection, $pageTitle); //Sanitize for SQL query
	$query = "SELECT * FROM PageEdits WHERE PageTitle = '$pageTitle' ORDER BY EditDate DESC LIMIT 1"; //Will return all info for most recent edit for PageTitle
	$result = mysqli_query($connection, $query);
	
	return (hasZeroRows($result) ? FALSE : mysqli_fetch_assoc($result));
}


/**	FUNCTION:	getRecentArticles
**	PURPOSE:	Returns an array object of the (x) most recently edited articles.
**	PARAMS:		mysqli connection
**	RETURN:		array of most recently edited articles
*/
function getRecentArticles($connection){

	$query = "SELECT * FROM (SELECT * FROM PageEdits ORDER BY EditDate DESC) AS Edits GROUP BY PageTitle ORDER BY EditDate DESC LIMIT 10";
	$pageRows = mysqli_query($connection, $query); //Get the 10 most recently created pages (by title)
	//Turn the returned rows into an array
	$pageList = array();
	$htmlSoFar = ""; //Initiate HTML string
	while ($pageRow = mysqli_fetch_assoc($pageRows)){
		$pageList[] = $pageRow;
	}
	
	return $pageList;
}

/**	FUNCTION:	getEditsByUser
**	PURPOSE:	Returns an array object of the (x) most recently edited articles.
**	PARAMS:		mysqli connection
**	RETURN:		array of most recently edited articles
*/
function getEditsByUser($connection, $username){

	$query = "SELECT * FROM PageEdits WHERE UserName = '$username' ORDER BY EditDate DESC";
	$pageRows = mysqli_query($connection, $query); //Get the 10 most recently created pages (by title)
	//Turn the returned rows into an array
	$pageList = array();
	$htmlSoFar = ""; //Initiate HTML string
	while ($pageRow = mysqli_fetch_assoc($pageRows)){
		$pageList[] = $pageRow;
	}
	
	return $pageList;
}

/** Function to return only the most current version of the selected page (Used for page displaying) */
function getMostRecentPageEdit($connection, $PageTitle){
	$pageTitle = sqlSanitize($connection, $pageTitle); //Sanitize for SQL query
	$query = "SELECT PageTitle, PageBody, MAX(EditDate) From PageEdits GROUP BY PageTitle"; //Will return all 
	
}

/** Checks to see if the returned set is empty or not. **/
function hasZeroRows($tableSet){
	return (mysqli_num_rows($tableSet) == 0);
}

/*
*checkIfAdmin() - Checks if a user is admin
*@param username - Username to be checked
*@return admin boolean
*/
function checkIfAdmin($connection, $username){
	$isAdmin = false;

	$qu = "SELECT admin FROM User WHERE userName='$username'";

	if($connection)
	{
		$allRows = mysqli_query($connection, $qu);

		while($oneRow = mysqli_fetch_assoc($allRows))
			{
				if($oneRow['admin'] == 1)
				{
					$isAdmin = true;
				}
			}
	}


	
	return $isAdmin;
}
?>