<?php
#########################################################################################
## 	Purpose: 		- Provides a series of utility functions to be used in radfordpedia.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	04 December 2017
##
##	Disclaimer:		Some functions used directly from previous classwork utils.php
##					- Of these, some may be credited directly to Ian Barland, reference
##					source: https://learn.radford.edu/d2l/le/content/114903/Home?1875050
#########################################################################################


/** Do an array lookup, or return a default value if item not found.
 * @param $arr The array to look up in.
 * @param $key The key to look up.
 * @param $dflt The default value to return, if $arr[$key] doesn't exist.
 * @return $arr[$key], or $dflt if $key isn't a key in $arr.
 */
function safeLookup($arr, $key, $dflt = null) {
	return (array_key_exists($key,$arr) ? $arr[$key] : $dflt);
}

/** Return $_POST[$key] (but don't generate a warning, if it doesn't exist). */
	function getPost($key,$dflt="") { 
	$formValue = safeLookup($_POST,$key,$dflt);
	return get_magic_quotes_gpc()&&is_string($formValue) ? stripslashes($formValue) : $formValue; 
}

/** Returns a value from $_GET, decodes from URL format, and strips slashes (if magic slashes exists) */
function getGet($key,$dflt="") {
	$formValue = safeLookup($_GET,$key,$dflt);
	//URL decode -> Removed (unnecessary, GET automatically decodes)
	return get_magic_quotes_gpc()&&is_string($formValue) ? stripslashes($formValue) : $formValue; 
}

/** Creates a hyperlink based on the provided page title **/
function linkPageByTitle($pageTitle, $text = FALSE, $edit = FALSE){
	$address = ($edit ? "edit-" : "view" ) . "page.php?page=" . urlencode($pageTitle);
	return "<A HREF='$address'>" . ($text ? $text : $pageTitle) . "</a>";
}


/** Takes an array of strings, returns HTML for an Unordered List **/
function stringsToUL( $itms ) {
	$lineItemsSoFar = "";
	foreach ($itms AS $key=>$itm) {
		$lineItemsSoFar .= "  <li>". (is_string($key) ? "$key: " : "")   ."$itm</li>\n";
	}
	return "<ul>\n" . $lineItemsSoFar . "</ul>\n";
}

/** strToHtml: quote a (raw) string to html. */
function strToHtml($str) { return nl2br(htmlspecialchars($str,ENT_QUOTES)); }

//################################################################################################################################
//#################################														##########################################
//#################################  ERROR CHECKING FUNCTIONS FOR PAGE SUBMISSION   	##########################################
//#################################														##########################################
//################################################################################################################################


/**	FUNCTION:	getTitleErrors
**	PURPOSE:	Server-side checking for errors related to page title input
**	PARAM:		Title of the page
**	RETURN:		string of error messages, or FALSE if none.
*/
function getTitleErrors($pageTitle){
	$errs = array(); //Array to hold any and all error messages
	//Check to see if pageTitle is a string
	if (!is_string($pageTitle)){
		$errs[] = "Page Title must be a string.";
	}

	//Test to see if is empty
	if (strlen($pageTitle) == 0){
		$errs[] = "Page title must not be blank.";
	}

	//Test for max length
	if ($pageTitle > 50){
		$errs[] = "Page title may not exceed 50 characters.";
	}

	return sizeOf($errs)===0  ?  false  :  implode($errs,"; ").".";
}

/**	FUNCTION:	getSyntaxErrors
**	PURPOSE:	Server-side checking for errors related to page body syntax
**	PARAM:		Body of the page
**	RETURN:		string of error messages, or FALSE if none.
*/
function getSyntaxErrors($pageBody){
	$errs = array();
	//Check Summary Tags (Require them?)
	// $errMsg = checkSummaryTags($pageBody);
	// if ($errMsg) $errs[] = $errMsg;

	$errMsg = checkHeaderTags($pageBody, 1);
	if ($errMsg) $errs[] = $errMsg;
	$errMsg = checkHeaderTags($pageBody, 2);
	if ($errMsg) $errs[] = $errMsg;
	$errMsg = checkHeaderTags($pageBody, 3);
	if ($errMsg) $errs[] = $errMsg;

	return sizeOf($errs)===0  ?  false  :  implode($errs,"; ").".";
}

/**	FUNCTION:	checkSummaryTags
**	PURPOSE:	Server-side checking for errors in summary tags
**	PARAM:		Body of page
**	RETURN:		string of error messages, or FALSE if none.
*/
function checkSummaryTags($pageBody){
	$openTagExp = '/\[(?i)summary\]/';
	$closeTagExp = '/\[\/(?i)summary\]/';
	//Possibly insert something to ensure no other tags inside the summary tags? At least for header tags.

	if (preg_match($openTagExp, $pageBody, $match)){
		// echo "Found the opening tag";
		// var_dump($match);
		if (!preg_match($closeTagExp, $pageBody)) return "Missing closing [/Summary] tag";
	} else{
		return "Missing opening [Summary] tag";
	}

	return false;
}

/**	FUNCTION:	checkHeaderTags
**	PURPOSE:	Server-side checking for errors related to header tags. Option to specify which level of header is checked (1,2,3)
**	PARAM:		Body of page
**	RETURN:		string of error messages, or FALSE if none.
*/
function checkHeaderTags($pageBody, $headerLevel = 1){
	if ($headerLevel > 3 || $headerLevel < 1){
		return "Server Side: Invalid header level specified.";
	}else{
		//Set the regexp per header level
		if ($headerLevel == 1){
			$openTagExp = '/\[(?i)h1\]/';
			$closeTagExp = '/\[\/(?i)h1\]/';
		}
		if ($headerLevel == 2){
			$openTagExp = '/\[(?i)h2\]/';
			$closeTagExp = '/\[\/(?i)h2\]/';
		}
		if ($headerLevel == 3){
			$openTagExp = '/\[(?i)h3\]/';
			$closeTagExp = '/\[\/(?i)h3\]/';
		}

	}

	//Calculate how many open and closing tags there are
	$numOpens = preg_match_all($openTagExp, $pageBody, $matches);
	$numCloses = preg_match_all($closeTagExp, $pageBody, $matches);
	if ($numOpens > $numCloses){
		return "Missing " . ($numOpens - $numCloses) . " closing h$headerLevel tags";
	} elseif ($numOpens < $numCloses){
		return "Missing " . ($numCloses - $numOpens) . " opening h$headerLevel tags";
	} else{
		return false;
	}
}



//################################################################################################################################
//#################################														##########################################
//#################################  PAGE DISPLAY FUNCTIONS TO MAKE WITH THE PRETTY 	##########################################
//#################################														##########################################
//################################################################################################################################


/** Function:	displayEditHelp
**	Purpose:	displays the usable functions and tags for editing pages
**	Param: 		None
**	Return:		HTML for the display of the editor help
*/
function displayEditHelp(){
	$helpStrng = <<<HELPSTR
	<fieldset><legend>Useful Tags</legend>
	[H1]First Level Header[/H1]<BR/>
	[H2]Second Level Header[/H2]<BR/>
	[H3]Third Level Header[/H3]<BR/>
	</fieldset>
HELPSTR;

	return $helpStrng;
}

/** Function: 	displayPage
**	Purpose:	Takes in a page title, looks up page, and returns the HTML for the body of the page.
**	Param:		PageTitle (String, not URL-Encoded)
**	Return:		HTML String for page. If for some reason page is not found, display error page.
*/
function displayPage($connection, $pageTitle){
	$pageInfo = findPage($connection, $pageTitle);
	$pageHTML = "";
	if (!$pageInfo){
		$pageHTML = "<H1>Sorry, page not found</H1><BR/>";
	} else{
		$pageHTML .= createTitleBar(safeLookup($pageInfo, "PageTitle"), safeLookup($pageInfo, "EditDate"), safeLookup($pageInfo, "UserName"));
		$pageHTML .= createPageBody(safeLookup($pageInfo, 'PageBody'));
	}
	return $pageHTML;
}


/**	Function:	createTitleBar
**	Purpose:	To create the HTML for the title bar of a page
**	Param:		PageTitle, Username, EditDate
**	Return:		HTML code for a title bar.
*/
function createTitleBar($Title, $Date, $User = "Unknown"){
	$Title = htmlspecialchars($Title);
	$User = htmlspecialchars($User);
	$htmlSoFar = "";
	$htmlSoFar .= "<H1>" . $Title . "</H1><BR/>"; //Add Title as H1
	$htmlSoFar .= "Last " . linkPageByTitle($Title, 'edited', TRUE) . " on " . $Date . " by " . $User . "<BR/>"; //Add last edit info
	$htmlSoFar .= "<HR/>"; //Add HR divider.
	return $htmlSoFar;
}

/**	Function:	createPageBody
**	Purpose:	To create the HTML for the body of a page
**	Param:		Pagebody
**	Return:		HTML code for the page's body
*/

function createPageBody($pageBody){
	$pageBody = nl2br(htmlspecialchars($pageBody)); //Basic page sanitization
	$pageBody = stripSummaryTags($pageBody);
	$htmlSoFar = $pageBody . "<HR/>"; //Add a divider for the end of the page
	//Convert tags to HTML
	$htmlSoFar = tagsToHTML($htmlSoFar);
	return $htmlSoFar;
}

/**	Function:	tagsToHTML
**	Purpose:	Converts any tags in the body text into useable HTML code.
**	Param:		PageBody
**	Return:		Converted text
*/
function tagsToHTML($pageText){
	$pageText = headersToHTML($pageText);
	return $pageText;
}


/**	Function:	headersToHTML
**	Purpose:	Converts any header tags into HTML tags
**	Param:		PageBody
**	Return:		Converted text
*/
function headersToHTML($pageText){
	$pageText = preg_replace('/\[(?i)h1\]/', "<H2>", $pageText); //Replace H1 open tag
	$pageText = preg_replace('/\[\/(?i)h1\]/', "</H2><HR/>", $pageText); //Replace H1 close tag, add <HR> tag for dividing sections.

	$pageText = preg_replace('/\[(?i)h2\]/', "<H3>", $pageText); //Replace H2 open tag
	$pageText = preg_replace('/\[\/(?i)h2\]/', "</H3>", $pageText); //Replace H2 close tag

	$pageText = preg_replace('/\[(?i)h3\]/', "<H4>", $pageText); //Replace H2 open tag
	$pageText = preg_replace('/\[\/(?i)h3\]/', "</H4>", $pageText); //Replace H2 close tag

	return $pageText;
}

/**	Function:	stripSummaryTags
**	Purpose:	Replaces the [SUMMARY] tags in the body with empty string.
**	Param:		PageBody
**	Return:		Converted text
*/
function stripSummaryTags($pageText){
	$pageText = preg_replace('/\[(?i)summary\]/', "", $pageText);
	$pageText = preg_replace('/\[\/(?i)summary\]/', "", $pageText);
	return $pageText;
}

/**	Function:	getSummaryText
**	Purpose:	Gets text between summary tags. Assumes correct spelling (For the moment)
**	Param:		PageBody
**	Return:		Summary text
*/
function getSummaryText($pageText, $maxLength = 150){
	return substr($pageText, 0, $maxLength);
	// $regexp = '/\[(?i)summary\](\s*.*\s*)\[\/summary\]/';
	// if (preg_match($regexp, $pageText, $match)){ //Find the text which matches from summary to /summary
	// 	return substr($match[1], 0, $maxLength);
	// }else {
	// 	return substr($pageText, 0, $maxLength);
	// }
}

/**	FUNCTION:	displayPageSummaryTile
**	PURPOSE:	To take in page information, and display a brief summary of the page (Starting with only title and editdate)
**	PARAM:		Array of page information
**	Return:		HTML Element for a DIV which displays the page information.
//////////////EDIT to use DIVs instead of tables for more/better control?
*/
function displayPageSummaryTile($pageInfo, $deleteButton = FALSE){
	/*$htmlSoFar = "<DIV class='summaryTile' 
		style='background-color: lightblue;
		border-style: dashed;
		border-width: 1px;
		width: 100%;
		height: 60px;
		text-align: center;'><TABLE border=1 style='table-layout: fixed'><TR>";*/
	$htmlSoFar = "<tr><td>" . linkPageByTitle(htmlspecialchars(safeLookup($pageInfo, 'PageTitle'))) . "</td>";
	/*$htmlSoFar .= "<TD width='400px' style='word-wrap: break-word'>" . getSummaryText(safeLookup($pageInfo, 'PageBody')) . "</TD>";*/
	$htmlSoFar .= "<td>Last Edited: " . safeLookup($pageInfo, 'EditDate') . " by " . safeLookup($pageInfo, 'UserName') . "</td>";
		if ($deleteButton){
			$htmlSoFar .= "<td><form action='account-handle.php' method='post'><input type='hidden' name='PageTitle' value='" . safeLookup($pageInfo, 'PageTitle') . "'>";
			$htmlSoFar .= "<input type='hidden' name='EditDate' value='" . safeLookup($pageInfo, 'EditDate') . "'>";
			$htmlSoFar .= "<input type='hidden' name='UserName' value='" . safeLookup($pageInfo, 'UserName') . "'>";
			$htmlSoFar .= "<input type='submit' name='deletepage' value='DELETE PAGE'></form></td></tr>";
		} else{
			$htmlSoFar .= "</tr>";
		}
		
	/*$htmlSoFar .= "</TR></TABLE></DIV>";*/
	return $htmlSoFar;
}


//Takes an array of pageRows (each row was gotten by mysqli_fetch_assoc) and turns it into a table of article summaries
function listArticles($pageList, $deleteButton = FALSE){
	$htmlSoFar = "<table>";
	// $htmlSoFar = "<table class='pageList'>";
	foreach ($pageList AS $page){
		$htmlSoFar .= displayPageSummaryTile($page, $deleteButton);
	}
	$htmlSoFar .= "</table>";
	return $htmlSoFar;
}

function randomPageURL()
{
	$pageURL = "";

	$dbhost = 'localhost';
	$dbuser = 'proj2';
	$dbpass = 'Security1#';
	$dbname = 'proj2';

	$connect = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
	if($connect)
	{
		$qu = "SELECT pageTitle FROM Pages ORDER BY RAND( ) LIMIT 1";
		$allRows = mysqli_query($connect, $qu);

		while($oneRow = mysqli_fetch_assoc($allRows))
			{
				$pageURL = urlencode($oneRow["pageTitle"]);
			}
	}


	mysqli_close($connect);

	return $pageURL;
}


/** Function to create the master layout for the page, with sidebar and top bar */
function makeMaster($username = FALSE)
	{
		require_once("db-connection.php");	
		$connection = db_connect_radfordpedia();
		$header = "<div id='headerTop'>" .
				  "<a href='viewpage.php'>\n".
				  "		<img id='radfordpedialogo' src='images/radfordpedialogo.png' alt='RadfordPedia Logo'>\n" .
				  "</a>\n" .
				  "		<img id='radfordpediabanner' src='images/radfordpediabanner.png' alt='RadfordPedia Banner'>\n" .
				  "<form id='search' action='searchHandle.php' method='post'> " . 
				  "		<button id='searchButton' type='submit'>Search</button>\n" .
				  "		<input id='searchBox' type='text' name='search'>\n" .
				  "</form>\n";
				  //Add login / Sign up links
				  if(safeLookup($_SESSION, 'username') == FALSE){ $header .= "		<span id='userInfo'><a href='login.php'>Login</a> OR <a href='registration.php'>Register</a></span>\n"; }else { $header .= "<a id='logoutButton' href='logout.php'>(LogOut)</a><a id='userDisplay' href='accountpage.php'>" . $_SESSION['username'] . "</a>\n";}
				  $header .= "</div>\n";

		$sideBar = "<div id='headerSideBar'>" .
				   "	<br />\n" .
				   "	<br />\n" .
				   "	<br />\n" .
				   "	<br />\n" .
				   "	<a id='homeButton' href='viewpage.php'>Home</a>\n".
				   "	<br />\n" .
				   "	<a id='createPageButton' href='edit-page.php'>Create New Page</a>\n".
				   "	<br />\n" .
				   "	<a id='randomButton' href='viewpage.php?page=" . randomPageURL() . "'>Random Page</a>\n";
				   	if(isset($_SESSION['username']))
				   	{
					   	if(checkIfAdmin($connection, $_SESSION['username']))
					   	{
					   		$sideBar .= "<br/><a id='adminButton' href='admin.php'>Admin</a>\n";
					   	}
				   	}

				   $sideBar .= "</div>\n";
		mysqli_close($connection);
		return $header . $sideBar;
	}

/*
*	Makes a post table for the admin page
*/
function getPostsTable()
{
		$dbhost = 'localhost';
		$dbuser = 'proj2';
		$dbpass = 'Security1#';
		$dbname = 'proj2';

		$connect = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

		$result = "<table>";
		$result .= 
		"<tr>
			<th>Post Title</th>
			<th>Delete</th>
		</tr>";
		
		if($connect)
		{
			$qu = "SELECT PageTitle FROM Pages;";
			$allRows = mysqli_query($connect, $qu);

			while($oneRow = mysqli_fetch_assoc($allRows))
			{
				$result .= 
				"<form action='adminPostsHandler.php' method='POST' id='pagesForm'>" .
				"<tr>" .
				"<td>" . "<input type='hidden' name='postTitle' value='" . $oneRow["PageTitle"] . "'>" . $oneRow["PageTitle"] . "</td>";
					
				$result .= "<td><button type='submit' name='delete' value='delete'>Delete</button></td>";
				$result .= "</tr>" . 
				"</form>";
			}
		}
		mysqli_close($connect);

		$result .= "</table>";

		return $result;
}

/** Makes a user table for the admin page. **/
function getUsersTable()
	{
		$dbhost = 'localhost';
		$dbuser = 'proj2';
		$dbpass = 'Security1#';
		$dbname = 'proj2';

		$connect = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

		$result = "<table>";
		$result .= 
		"<tr>
			<th>Username</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Admin</th>
			<th>Delete</th>
		</tr>";
		
		if($connect)
		{
			$qu = "SELECT fName, lName, userName FROM User;";
			$allRows = mysqli_query($connect, $qu);

			while($oneRow = mysqli_fetch_assoc($allRows))
			{
				$result .= 
				"<form action='adminUserHandler.php' method='POST' id='usersForm'>" .
				"<tr>" .
					"<td>" . "<input type='hidden' name='userName' value='" . $oneRow["userName"] . "'>" . $oneRow["userName"] . "</td>" .
					"<td>" . $oneRow["fName"] . "</td>" .
					"<td>" . $oneRow["lName"] . "</td>";
					
					
					if(checkIfAdmin($connect, $oneRow["userName"]))
					{
						$result .= "<td><button type='submit' name='admin' value='admin' width='100'>Remove Admin</button></td>";
					}
					else
					{
						$result .= "<td><button type='submit' name='admin' value='admin' width='100'>Make Admin</button></td>";
					}

				$result .= "<td><button type='submit' name='delete' value='delete'>Delete</button></td>";
				$result .= "</tr>" . 
				"</form>";
			}
		}
		mysqli_close($connect);

		$result .= "</table>";

		return $result;
	}
?>