<?php
#########################################################################################
## 	Purpose: 		Displays the specified page from the radfordpedia database.
##					If no page provided (with a GET array), displays landing page of
##						recently created articles.
##
##	Author(s):		Harry "Alan" Frank, Jr
##	Last Modified: 	04 December 2017
#########################################################################################

//Error reporting and includes
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
require_once('radfordpedia-utils.php');
require_once('db-connection.php');
session_start();
$username = safeLookup($_SESSION, 'username');


//Retrieve page from GET
$page = getGet('page');
//Connect to database
$dbConn = db_connect_radfordpedia();

//Check to see if page was provided in URL
if ($page){
	if (pageExists($dbConn, $page)){ //Page is found in the database
		//Retrieve page information from DB
		$pageTitle = $page;
		$pageBody = displayPage($dbConn, $page); //Will look up the page in the database, and return HTML for the page.
	} else{
		$pageTitle = "Page Summary";
		$pageBody = "Page <B>$page</B> not found. How about one of these?";
		$pageBody .= listArticles(getRecentArticles($dbConn));
		$pageBody .= "<BR><A HREF='edit-page.php'>Or, edit your own!</A><BR/>";
	}
} else{
	//Display summary page (summarypage function maybe?)
	$pageTitle = "Page Summary";
	$pageBody = "<h1>Welcome to RadfordPedia! Check out some of these recently edited articles!</H1><BR/>";
	$pageBody .= listArticles(getRecentArticles($dbConn));
	$pageBody .= "<BR><A HREF='edit-page.php'>Or, edit your own!</A><BR/>";
}

mysqli_close($dbConn);//CLOSE CONNECTION!
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="styles.css" type="text/css">
	<title><?php echo $pageTitle; ?></title>
</head>
<body>
	<?php echo makemaster(); ?>
	<?php echo $pageBody; ?>
	<A HREF="viewpage.php">BACK TO LANDING</A>
</body>
</html>