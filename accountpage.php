<?php
#########################################################################################
## 	Purpose: 		Show and account
##
##	Author(s):		Ben Adams
##	Last Modified: 	12 December 2017
#########################################################################################

//Error reporting and includes
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
require_once('radfordpedia-utils.php');
require_once('db-connection.php');
session_start();

$username = safeLookup($_SESSION, 'username');



//Connect to database
$dbConn = db_connect_radfordpedia();

//Check to see if user is in session
if ($username){
	$qry = "SELECT * FROM User WHERE userName ='".$username."'";
	$result = mysqli_fetch_assoc(mysqli_query($dbConn,$qry));


	$fName = $result['fName'];
	$lName = $result['lName'];
	$userName =  $result['userName'];
	$email =  $result['email'];

	$pageTitle = "UserPage: $userName";
	$pageBody = "Name: " .$fName ." ". $lName ."<div>Username ". $userName . "<div>Email ". $email;
	$pageBody .= "<div>";
	$pageBody .= "<h2>Recently Edited Pages:</h2>";

// 	// Code is to show acount edits but isnt working
	$pageInfo = getEditsByUser($dbConn, $username);
	$pageBody .= listArticles($pageInfo, TRUE);
// 	 $qry = "SELECT PageTitle,EditDate FROM PageEdits WHERE UserName ='".$username."'";
// 	$result = mysqli_query($dbConn,$qry);
	

	
}

mysqli_close($dbConn);//CLOSE CONNECTION!
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="styles.css" type="text/css">
	<title><?php echo $pageTitle; ?></title>
</head>
<body>
	<?php echo makemaster(); ?>
	<?php echo $pageBody, "<br/>"; ?>
	<A HREF="viewpage.php">BACK TO LANDING</A>
</body>
</html>