<?php
error_reporting(E_ALL);
ini_set('display_errors',true); 
ini_set('display_startup_errors',true);
require_once('radfordpedia-utils.php');
require_once('db-connection.php');

//Start session
session_start();

//Check to see if user and password exist in DB

$connection = db_connect_radfordpedia();

//Query DB for username and PW
$successfulSignon = userLogin($connection, safeLookup($_POST, 'username'), safeLookup($_POST, 'password'));
$pageBody="";
if ($successfulSignon){	
	echo "session started";
	$_SESSION['username'] = safeLookup($_POST, 'username');
	header('Location: viewpage.php');
} else{
	//Here will be the error messages page for unsuccessful login.
	unset($_SESSION);
	session_destroy();
	session_write_close();
	header('Location: login.php?logon=fail');
}


?>